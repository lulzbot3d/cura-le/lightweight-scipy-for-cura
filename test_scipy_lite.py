import numpy
import scipy.spatial

# Test with 2-D points

def test_convex_hull():
    points = numpy.random.rand(30, 2)   # 30 random points in 2-D

    cura2_hull = scipy.spatial.ConvexHull(points)

    print ("Points in Set: ",              len(points))
    print ("Shape of Cura2 points: ",      cura2_hull.points.shape)
    print ("Shape of Cura2 ConvexHull: ",  cura2_hull.vertices.shape)

    cura2_points = numpy.take(cura2_hull.points, cura2_hull.vertices, axis=0)
    cura2_points.sort(axis=0);

    # Test with 3-D points, right now we simply ignore the third axis

    points = numpy.random.rand(30, 3)   # 30 random points in 3-D

    cura2_hull = scipy.spatial.ConvexHull(points)

    print ("Points in Set: ",              len(points))
    print ("Shape of Cura2 points: ",      cura2_hull.points.shape)
    print ("Shape of Cura2 ConvexHull: ",  cura2_hull.vertices.shape)

    cura2_points = numpy.take(cura2_hull.points, cura2_hull.vertices, axis=0)
    cura2_points.sort(axis=0);

def test_qerror():
    # Test that QhullError is defined
    print ("QhullError: ", scipy.spatial.qhull.QhullError)

test_convex_hull();
test_qerror();