Lightweight SciPy for Cura
==========================

This library implements a shim for Cura2 that allows compilation of Cura without
SciPy, using alternative implementations of the convex hull algorithm that Cura
uses internally. The motivation for doing so is that SciPy is a large package
that can be very difficult to compile.

This library presents itself as "scipy" so that the Cura source can remain
unmodified, but this library is not a substitute for scipy for general uses.

Currently this library will allow Cura to work using one of the following two
choices:

* Using the Pyhull library (a binding to the QHull library, written in C)
* Using experimental Python-only algorithms.

The first option is the only practical option right now. The Python
chull3d is too slow for loading any reasonably sized model into Cura,
but the code is included here in case someone wants to take a shot at
optimizing it.

Algorithms Used:
================

Pyhull:
* http://pythonhosted.org/pyhull/
* License: MIT

QHull:
* http://www.qhull.org/
* License: MIT-like (https://raw.githubusercontent.com/qhull/qhull/master/COPYING.txt)

chull2d.py:
* http://code.activestate.com/recipes/66527-finding-the-convex-hull-of-a-set-of-2d-points/
* License: PSF License

chull3d.py:
* http://michelanders.blogspot.com/2012/02/3d-convex-hull-in-python.html
* https://github.com/varkenvarken/blenderaddons/blob/master/chull.py
* License: GPLv2+