__all__ = ['ConvexHull','qhull']

import numpy

try:
    import pyhull.convex_hull

    class ConvexHull:
        def __init__(self, points):
            self.points = points
            hull = pyhull.convex_hull.ConvexHull(points)
            if(points.shape[1] == 2):
                # For 2D contours, hull contains a list
                # list of tuples representing unsorted
                # edges. Cura expects an ordered list of
                # vertices in sequence. We do the
                # conversion here.
                def findAdjacentVertex(vertex):
                    for v in hull.vertices:
                        if(v[0] == vertex):
                            return v[1]
                vertices = []
                first    = hull.vertices[0][0]
                next     = hull.vertices[0][1]
                while next != first:
                    vertices.append(next)
                    next = findAdjacentVertex(next)
                vertices.append(next)
                # Convert into a Numpy array
                self.vertices = numpy.asarray(vertices)
            elif(points.shape[1] == 3):
                # For 3D meshes, Cura expects an unordered
                # list of points. So we flatten the tuples
                # and deduplicate them.
                self.vertices = numpy.unique(numpy.asarray(hull.vertices).flatten())
            else:
                raise Exception("ConvexHull: Dimension must be 2 or 3");

except ImportError:
    from .chull2d import convexHull
    from .chull3d import Hull, Vector

    print ("Using chull2d.py and chull3d.py for convex hull operations");
    print ("WARNING: This is too slow for all practical purposes!");

    class ConvexHull:
        def __init__(self, points):
            self.points = points
            if(points.shape[1] == 2):
                print("Calling chull2d");
                # Use chull2d:
                # Convert numpyArray to list of tuples
                tuples = []
                for row in range(points.shape[0]):
                    tuples.append((points[row,0],points[row,1]));
                # Generate the convex hull
                points = convexHull(tuples)
                # Convert list of tuples into numpyArray
                self.points   = numpy.asarray(points)
                self.vertices = numpy.arange(len(points))
            elif(points.shape[1] == 3):
                # Use chull3d:
                # Convert numpyArray to list of Vector()s
                vectors = []
                for row in range(points.shape[0]):
                    vectors.append(Vector(points[row,0],points[row,1],points[row,2]));
                vectors = list(set(vectors))
                # Compute the convex hull
                hull = Hull(vectors)
                # Convert list of Faces into numpyArray of points
                points = []
                for f in hull.faces:
                    points.append((f.vertex[0].v.x,f.vertex[0].v.y,f.vertex[0].v.z));
                    points.append((f.vertex[1].v.x,f.vertex[1].v.y,f.vertex[1].v.z));
                    points.append((f.vertex[2].v.x,f.vertex[2].v.y,f.vertex[2].v.z));
                points = list(set(points));
                self.points   = numpy.asarray(points)
                self.vertices = numpy.arange(len(points))
            else:
                raise Exception("ConvexHull: Dimension must be 2 or 3");

class qhull:
    QhullError = -1;
