
__all__ = ['svd']

from numpy.linalg import svd as numpy_svd

def svd(a, full_matrices=1, compute_uv=1):
  return numpy_svd(a, full_matrices, compute_uv)